import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { BrandsComponent } from './brands/brands.component';
import { OffersComponent } from './offers/offers.component';
import { ServicesComponent } from './services/services.component';
import { HomeComponent } from './home/home.component';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { CountDownModule } from  'ng6-countdown/dist/ng6-countdown-lib';
import 'hammerjs';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { EventsComponent } from './events/events.component';
import { AboutComponent } from '../about/about.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: '../about-us', component: AboutComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BrandsComponent,
    OffersComponent,
    ServicesComponent,
    HomeComponent,
    EventsComponent,
    AboutComponent
    ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule,
    CountdownTimerModule.forRoot(),
    CountDownModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(appRoutes),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
