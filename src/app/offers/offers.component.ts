import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Offer } from '../model/offer.model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
// import {CountdownComponent} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OffersComponent implements OnInit {

  isCustomTemplate = true;
  days;
  hours;
  minutes;
  seconds;
  backgroundColor = 'red';
  textColor = 'black';
  
  closeResult: string;
  public abc: boolean=true;
  public offers :any = [];
  selectedOffer:Offer;
  constructor(private modalService: NgbModal) { }

  ngOnInit() {

    let offer1:Offer={
    description:"Weekend Offer on Impex Products",
    imgUrl:"../../assets/img/offers/impex.jpg",
    fromDate:new Date,
    toDate:new Date,
    name:"Impex Offer"
    };

    let offer2:Offer={
    description:"Cristmas Offer on Electronics Products",
    imgUrl:"../../assets/img/offers/impex.jpg",
    fromDate:new Date('2019-07-05'),
    toDate:new Date('2019-07-05'),
    name:"Flexi Offer"
  };

  let offer3:Offer={
    description:"Flexi Offer on Electronics Products",
    imgUrl:"../../assets/img/offers/impex.jpg",
    fromDate:new Date,
    toDate:new Date,
    name:"Flexi Offer"
  };


    this.offers.push(offer1);
    this.offers.push(offer2);
    this.offers.push(offer3);
  }
  open(content,offer) {
    console.log(content);
    this.selectedOffer=offer;
    console.log(this.selectedOffer.fromDate);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  onDaysChangedOfferModal(days) {
    this.days  =  days;
    }
     
    onHoursChangedOfferModal(hours) {
    this.hours  =  hours;
    }
     
    onMinutesChangedOfferModal(minutes) {
    this.minutes  =  minutes;
    }
     
    onSecondsChangedOfferModal(seconds) {
    this.seconds  =  seconds;
    }
  
}
