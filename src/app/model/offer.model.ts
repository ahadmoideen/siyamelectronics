export class Offer {
    imgUrl: string;
    name: string;
    description: string;
    fromDate: Date;
    toDate: Date;
  }