import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbAccordionConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { AnimationConfig, ICarouselConfig } from 'angular4-carousel';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation:ViewEncapsulation.None
  // providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
 public images=[];
 constructor(config: NgbCarouselConfig) {
  config.interval = 2000;
  config.wrap = true;
  config.keyboard = true;
  config.pauseOnHover = true;
}

  ngOnInit() {
    this.images=["../../assets/img/carousel/slide-01-light.jpg",
    "../../assets/img/carousel/slide-02-light.jpg"];
  }
  

}
