import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Brand} from '../model/brand.model';
import {Observable} from 'rxjs';
  import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss'],
  providers: [NgbCarouselConfig]
})
export class BrandsComponent {

  images=["http://bloquo.cc/img/works/1.jpg",
    "http://bloquo.cc/img/works/1.jpg",
    "http://bloquo.cc/img/works/2.jpg",
    "http://bloquo.cc/img/works/3.jpg",
    "http://bloquo.cc/img/works/4.jpg"];
    constructor(config: NgbCarouselConfig) {
      config.interval = 1500;
      config.wrap = true;
      config.keyboard = false;
      config.pauseOnHover = false;
    }
  
}
